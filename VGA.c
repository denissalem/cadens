/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of CADENS.
 *
 * CADENS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CADENS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CADENS. If not, see <http://www.gnu.org/licenses/>.
*/

#include "VGA.h"
#include <conio.h>
#include <dos.h>
#include <i86.h>
 
png_color colors[256] = {0};
unsigned char color_count = 0;

void text_mode() {
    union REGS regs;
    regs.h.ah = 0x00;
    regs.h.al = 0x03; /* text mode is mode 3 */
    int86(0x10,&regs,&regs);
}

void mode13() {
    union REGS regs;
    regs.h.ah = 0x00;  /* function 00h = mode set */
    regs.h.al = 0x13;  /* 256-color */
    int86(0x10,&regs,&regs);
    
}

void update_palette() {
    unsigned int i = 0;
    outp(0x03c8, 0);
    
    for(i=0; i<256; i++) { 
      outp(0x03c9, colors[i].red >> 2);
      outp(0x03c9, colors[i].green >> 2);
      outp(0x03c9, colors[i].blue >> 2 );
    }
}

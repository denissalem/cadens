CC         = wcc
LINKER     = wcl
CFLAGS     = -zq -ml -bt=dos -oilrtfm -wx -3
WATCOM		 = /home/denis/git/open-watcom-v2/rel
DEPS			 = $WATCOM/lib286/dos/zlib.lib $WATCOM/lib286/dos/libpng.lib

CADENS		 = cadens.exe

OBJS			 = VGA.o      &
             cadens.o		&
             assets.o


all : $(CADENS)

.c.o:
         $(CC) $(CFLAGS) $[@

$(CADENS) : $(OBJS)
         $(LINKER) $DEPS $(OBJS) -fe=$(CADENS)

clean : .SYMBOLIC
         rm -f $(OBJS)
         rm -f *err
         rm -f $(CADENS)

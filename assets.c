/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of CADENS.
 *
 * CADENS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CADENS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CADENS. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <malloc.h>

#include "assets.h"
#include "VGA.h"

int num_palette = 0;
png_colorp palette = NULL;

int read_png(FILE * f, Asset * image) {
    unsigned char sig[8];
    png_structp png_ptr;
    png_infop info_ptr;
    png_uint_32 rowbytes;
    png_bytep * rows;

    png_uint_32 i;
    png_uint_32 width, height;
    fread(sig, 1, 8, f);
    if (!png_check_sig(sig, 8)) {
        printf("png_check_sig failed.\n");
        return 1;
    }
    
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr) {
        printf("png_create_read_struct failed.\n");
        return 2; /* out of memory */
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr) {
        printf("png_create_info_struct failed.\n");
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        return 3; /* out of memory */
    }


    png_init_io(png_ptr, f);
    
    png_set_sig_bytes(png_ptr, 8);
        
    png_read_info(png_ptr, info_ptr); // stack overflow

    png_get_IHDR(
        png_ptr,
        info_ptr,
        &width,
        &height,
        &image->bit_depth,
        NULL,
        NULL,
        NULL,
        NULL
    );

    image->width = (unsigned int) width;
    image->height = (unsigned int) height;
    
    rowbytes = png_get_rowbytes(png_ptr, info_ptr);
    rows = (png_bytep *) malloc(sizeof(png_bytep) * height);
    image->data = (unsigned char *) malloc(rowbytes * height);
    
    if ( image->data == NULL) {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        return 4;
    }

    for (i = 0;  i < image->height;  ++i) {
        rows[i] = image->data + i*rowbytes;
    }
    
    png_read_image(png_ptr, rows);
    
    png_get_PLTE(
        png_ptr,
        info_ptr,
        &palette,
        &num_palette
    );
  
    free(rows);

    return 0;
}

int load_asset(Asset * image) {
    png_uint_32 i, j;
    int alpha_index;
    int match = 0;
        
    png_color green = {0};
    
    green.green = 0xFF;

    for (i=0; i < num_palette; i++) {
      if (!CMP_COLOR(palette[i], green)) {
          alpha_index = i;
          palette[i].green = 0;
          break;
      }
    }
    
    for (i=0; i < image->height * image->width; i++) {
        if (image->bit_depth == 4) {
            // THIS IS WRONG : it will skip some colors
            //~ if (GET_COLOR_INDEX_4BITS(image, i) == 0) {
                //~ SET_COLOR_INDEX_4BITS(image, i, alpha_index);
            //~ }
            //~ else if (GET_COLOR_INDEX_4BITS(image, i) == alpha_index) {
                //~ SET_COLOR_INDEX_4BITS(image, i, 0); 
            //~ }
        }
        else if (image->bit_depth == 8) {
            if (image->data[i] == 0) {
                image->data[i] = alpha_index;
            }
            else if (image->data[i] == alpha_index) {
                image->data[i] = 0;
            }
        }
    }

    for(j= 0; j < num_palette; j++) {
        for (i = 0; i < color_count; i++) {
            if (!CMP_COLOR(colors[i], palette[j])) {
                match = 1;
                break;
            }
        }
        if (!match) {
            colors[color_count++] = palette[j];
        }
    }

    return 0;
}

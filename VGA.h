/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of CADENS.
 *
 * CADENS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CADENS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CADENS. If not, see <http://www.gnu.org/licenses/>.
*/

#include <png.h>

#ifndef VGA_H
#define VGA_H

extern png_color colors[256];
extern unsigned char color_count;

void text_mode();

void mode13();

void update_palette();

#endif 

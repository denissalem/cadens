/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of CADENS.
 *
 * CADENS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CADENS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CADENS. If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <malloc.h>
#include <string.h>

#include <conio.h>

#include "assets.h"
#include "cadens.h"
#include "levels.h"
#include "VGA.h"

void draw_loop(Asset * image) {
    byte far *VGA = (byte far*) 0xA0000000L;
    byte far *VGA_double_buffer = (byte far*) malloc(64000 * sizeof(byte));
    long unsigned int x,y;
        
    memset(VGA_double_buffer, 0, 64000);
    memset(VGA, 0, 64000);

    while (!kbhit()) {
        for (y=0; y<image->height && y < 200; y++) {
            for (x=0; x<image->width && x < 320; x++) {
                if (image->bit_depth == 4) {
                    if (x%2) {
                        VGA_double_buffer[x+y*320] = (image->data[(x>>1)+ ((y*image->width) >> 1)]) & 0xF;
                    }
                    else {
                        VGA_double_buffer[x+y*320] = (image->data[(x>>1)+ ((y*image->width)>>1)] ) >> 4;
                    }
                }
                else if(image->bit_depth == 8) {
                    VGA_double_buffer[x+y*320] = image->data[x+ y*image->width] ;
                }
            }
        }
        memcpy(VGA,VGA_double_buffer,64000);
    }
    free(VGA_double_buffer);
}

int main () {
  FILE * f = NULL;
  int ret = 0;
  Asset sprite = {0};
  f = fopen("assets/0000.png", "rb");
  
  if (f == NULL) {
      printf("Can't open file!\n");
      return -1;
  }

  if(ret = read_png(f, &sprite)) {
      printf("erreur %d: Can't load asset!\n", ret);
      return -1;
  }
  
  load_asset(&sprite);
  
  fclose(f);
  
  mode13();
  
  update_palette();
  
  draw_loop(&sprite);
  text_mode();
  
  return 0;
}

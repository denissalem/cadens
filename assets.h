/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of CADENS.
 *
 * CADENS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CADENS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CADENS. If not, see <http://www.gnu.org/licenses/>.
*/


#ifndef ASSETS_H
#define  ASSETS_H

#include "cadens.h"
#include <png.h>

typedef struct Asset_t {
    png_uint_32 width;  
    png_uint_32 height;
    int bit_depth;
    byte * data;
} Asset;

int read_png(FILE * f, Asset * image);

int load_asset(Asset * image);

extern int num_palette;
extern png_colorp palette;

#endif

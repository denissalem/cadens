/*
 * Copyright 2022 Denis Salem
 *
 * This file is part of CADENS.
 *
 * CADENS is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * CADENS is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with CADENS. If not, see <http://www.gnu.org/licenses/>.
*/
#ifndef CADENS_H
#define CADENS_H

typedef unsigned char byte;

#define CMP_COLOR(color1, color2) (color1.red != color2.red || color1.green != color2.green || color1.blue != color2.blue)


// THE FOLLOWING MACROS ARE WRONG
#define GET_COLOR_INDEX_4BITS(image, i) ( i%2 ? image->data[i >> 1] & 0xF : image->data[i >> 1] >> 4)
#define SET_COLOR_INDEX_4BITS(image, i, index) \
  if (i%2) { \
      image->data[i >> 1] &= 0xF0; \
      image->data[i >> 1] |= (index) & 0xF; \
  } else { \
      image->data[i >> 1] &= 0x0F; \
      image->data[i >> 1] |= (index << 4) & 0xF0; \
  }
#endif
